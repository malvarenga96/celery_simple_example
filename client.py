"""
This is a simple script which works as a HTTP client.

To send http request to 'celery_api_project' in order to simulate some clients
trying to approximate the PI value.
"""

import random

import requests


BASE_URL = 'http://localhost:8000/api/pi/'

DATA_STORAGE = {}


def send_requests(n, url, data={}):
    responses = [
        requests.get(
            url=url,
            json=data,
        )
        for _ in range(n)
    ]

    return responses


def get_task_by_id(task_id):
    return send_requests(1, BASE_URL+task_id+'/result/')


def create_tasks(n, millions):
    responses = send_requests(n, BASE_URL, {'millions': millions})
    store_data(DATA_STORAGE, responses)


def refresh_tasks(storage):
    for task in storage:
        response = get_task_by_id(task)
        store_data(storage, response)


def clean_response(responses):
    return map(lambda response: response.json(), responses)


def store_data(storage, responses):
    responses_payloads = clean_response(responses)
    data = {
        element['task_id']: element
        for element in responses_payloads
    }
    storage.update(data)


def show_tasks(data):
    print(*data.values(), sep='\n')


class Menu():
    def __init__(self):
        self.message = """
            0.Configure a custom request
            1.Refresh status of tasks stored
            2.Show tasks stored
            3.Send a random request
            4.Show Menu
        """
        self.operations = [
            self.__generate_pi,
            self.__refreh_tasks,
            self.__show_tasks,
            self.__random_tasks,
            self.show_menu,
        ]

    def show_menu(self):
        print(self.message)

    def execute(self, option):
        self.operations[option]()

    def __generate_pi(self):
        n = int(input('Number of requests to send: '))
        millions = int(
            input('Millions of points to generate (Type 1 for 1 million): ')
        )

        responses = send_requests(n, BASE_URL, {'millions': millions})
        store_data(DATA_STORAGE, responses)

    def __refreh_tasks(self):
        refresh_tasks(DATA_STORAGE)

    def __show_tasks(self):
        show_tasks(DATA_STORAGE)

    def __random_tasks(self):
        n = int(input('Number of requests to send: '))
        max_rounds = int(
            input('Max number of points to generate (Type 5 for 5 millions): ')
        )
        for _ in range(n):
            millions = random.randint(1, max_rounds)
            responses = send_requests(1, BASE_URL, {'millions': millions})
            store_data(DATA_STORAGE, responses)


if __name__ == "__main__":
    print('**********WARNING!!!!!!!!!!!!!!!!!\n'*2)
    print(
        'When the program ask you for a NUMBER OF POINTS to generate',
        'Type just 1, 2, 5 or any other single integer because',
        'It will be multiplied by 1 million. Try typing 80 or 100 if you want',
        sep='\n',
    )
    input('Press any key to continue...')
    menu = Menu()
    menu.show_menu()
    try:
        while True:
            option = int(input('Type a option: '))

            menu.execute(option)
    except (IndexError, ValueError, TypeError):
        print('Bye bye!')
