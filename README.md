# Simple Django API with Celey
The purpose of this project is to show how a simple Django API can be integrated with Celery in order to manage asynchronous tasks.

## Installation
To run this project is recommended that you have pipenv in you python installation and docker-compose available.

- Installing dependencies `pipenv install --dev`.
- Create a  `.env` You can use  `.env.example` as a template and just add a SECRET_KEY.

## Run the project
- To build the environment  `docker-compose build`.
- To run the environment  `docker-compose up`.

## Send requests
There are two endpoints:

### 1. Estimate PI value (create a new task)
**Endpoint:** http://localhost:8000/api/pi/
**Payload:** 
```json
{
    "millions": 15
}
```
**INFO**: the number 15 in the payload will be multiplied by 1000000 (1 million). It represents the amount of points to generate, in this case we are generating 15 millions of points.

### 2. Get task state by id
**Endpoint:** http://localhost:8000/api/pi/{task_id}/result/

## Use a client to send many request
- Enable your virtual environment `pipenv shell`.
- Run the client script `python client.py`.
- IMPORTANT: Read the warning that appears in console.

## Monitoring
**Open your browser:** http://localhost:5555/dashboard

Feel free to give me feedback about the code.
malvarenga@applaudostudios.com
