import json

from django.http import JsonResponse
from celery.result import AsyncResult

from .tasks import obtain_PI_value_task


def obtain_PI_value_view(request):
    body = request.body
    data = json.loads(body) if body else {}

    millions = data.get('millions', 1)

    # Create task and add it to be processed
    task = obtain_PI_value_task.delay(millions*1000000)

    payload = {
        'task_id': task.id,
        'task_status': task.status,
        'millions': millions
    }
    return JsonResponse(payload, status=202)


def obtain_task_state_view(request, task_id):
    try:
        task = AsyncResult(task_id)
        payload = {
            'task_id': task.id,
            'task_status': task.state,
            'task_result': task.result,
        }
    except TimeoutError:
        return JsonResponse({'error': 'Error with wroker service'}, status=500)

    return JsonResponse(payload, status=200)
