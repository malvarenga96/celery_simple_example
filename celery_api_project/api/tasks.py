import random

from celery import shared_task


@shared_task
def obtain_PI_value_task(n):
    return __get_PI(n)


def __get_PI(n):
    """"Use montecarlo method to obtain PI Number"""
    M = 0
    for i in range(n):
        x = random.uniform(-1, 1)
        y = random.uniform(-1, 1)

        if x**2 + y**2 < 1:
            M += 1
    PI = 4*M / n
    return PI
