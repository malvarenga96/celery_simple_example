from django.urls import path

from .views import obtain_PI_value_view, obtain_task_state_view

urlpatterns = [
    path(
        'pi/',
        obtain_PI_value_view,
        name='get-pi',
    ),

    path(
        'pi/<str:task_id>/result/',
        obtain_task_state_view,
        name='get-state',
    ),
]
