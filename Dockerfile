FROM python:3.8

RUN pip install --upgrade pip
RUN pip install pipenv

WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY . .

RUN pipenv lock --requirements > requirements.txt
RUN pip install -r requirements.txt
